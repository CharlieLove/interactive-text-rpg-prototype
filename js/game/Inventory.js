﻿class Inventory {
	constructor() {
		this.max_length = 30;
		this.encumberance_min = 70;

		this.cur_weight = 0;
		this.cur_items = [];
	}

	// take from ground
	take(index) {
		var item = cur_location.cur_room.items[index];

		if (this.cur_items.length < 30) {
			this.cur_items.push(item);

			// remove from ground
			// Find and remove item from an array
			if (index != -1) {
				cur_location.cur_room.items.splice(index, 1);

				// add item to room
			}

			// refresh ground item display
			cur_location.show_ground_items();

			// refresh inventory display
            this.show_inventory_items();
		}
	}

    // drop on ground
	drop(index) {
		var item = cur_inv.cur_items[index];

		// Find and remove item from an array
        if (index != -1) {
            cur_inv.cur_items.splice(index, 1);

            // check if the current room has items property,
            // if not add it
            if (cur_location.cur_room["items"] == null) {
                cur_location.cur_room["items"] = []
            }
            // add item to room
            cur_location.cur_room.items.push(item);

            // tell player what they've picked up
            //$("#txt_scrn").append("<hr>You drop " + item.name + ".<hr>");
		}

		// refresh ground item display
		cur_location.show_ground_items();

		// refresh inventory display
		this.show_inventory_items();
	}

	show_inventory_items() {
		var inv_items = cur_inv.cur_items;
		$('#inv_row').html('');
		for (var i = 0; i < inv_items.length; i++) {
            var button_open = '<div class="btn btn-outline-danger" href="#" role="button" onclick="cur_inv.drop(' + i + ')" id="inv_item_' + i + '" data - toggle="dropdown" aria - haspopup="true" aria - expanded="false" style = "width: 100%" > ';
            var button_text = "<b>Drop</b> <br>" + inv_items[i].name.charAt(0).toUpperCase() + inv_items[i].name.slice(1);;
			var button_close = '</div>';
			var button_finished = "<div class='col-6' style='padding:10px'>" + button_open + button_text + button_close + "</div>";

			$('#inv_row').append(button_finished);
		}
	}
}

let cur_inv = new Inventory();
