﻿const items_db_json = {
	"basic": {
		"i_b_egg": {
			"name": "egg",
			"description": "A single uncooked egg.",
			"weight": 0.125,
			"value": 5,
			"stackable": false
		},
		"i_b_wheat": {
			"name": "wheat",
			"description": "A bundle of wheat.",
			"weight": 1,
			"value": 5,
			"stackable": false
		},
		"i_b_bucket": {
			"name": "bucket",
			"description": "A wooden bucket.",
			"weight": 1,
			"value": 10,
			"stackable": false
		},
		"i_b_milk": {
			"name": "bucket of milk",
			"description": "A bucket of cow's milk.",
			"weight": 40,
			"value": 40,
			"stackable": false
        },
        "i_b_pot": {
            "name": "pot",
            "description": "A ceramic pot.",
            "weight": 10,
            "value": 2,
            "stackable": false
        },
        "i_b_potflour": {
            "name": "pot of flour",
            "description": "A ceramic pot full of flour.",
            "weight": 10,
            "value": 7,
            "stackable": false
        }
	},
	"consumable": {
		
	}
}