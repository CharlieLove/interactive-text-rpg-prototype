﻿const npcs_initial_db_json = {
    "npc_q_questman": {
        "name": "Quest Man",
        "flavor": "is standing in the corner, concerned at the state of your code.",
        "description": "A man who tests quests.",
        "progress": "0",
        "dlg_tree": {
            "0": {
                "text": "I test the quests. You are now quest progress 1.",
                "progress": "1"
            },
            "1": {
                "text": "You'll gain quest 2 once you give me a bucket of milk, an egg, and a pot of flour.",
                "requirement": ["bucket of milk", "egg", "pot of flour"],
                "progress": "2"
            },
            "2": {
                "text": "Oh, looks like you've got the stuff I need. Congrats, you've completed the quest! I've also removed those items from your inventory.",
                "progress": "3"
            },
            "3": {
                "text": "I've got nothing for you now. Thanks for the stuff though."
            }
        }
    }
}

var npcs_user_db_json = npcs_initial_db_json;