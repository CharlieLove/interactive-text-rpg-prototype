﻿// Purely for setting up a room's initial conditions and items
// This is where room information is typed in
const rooms_initial_json = {
	"rm_lumb_court":
	{
		"name": "Lumbridge Castle: Courtyard",
		"description": "Two fountains, a guard, blah blah blah",
		"objects": ["fountains", "guard"],
		"exits": {
			"west":"rm_lumb_mainhallway"
		}
	},
	"rm_lumb_mainhallway":
	{
		"name": "Lumbridge Castle: Main Hallway",
		"description": "Paintings, ex-monarchs, blah blah",
		"exits": {
			"southwest": "rm_lumb_kitchen",
			"east": "rm_lumb_court"
		}
	},
	"rm_lumb_kitchen":
	{
		"name": "Lumbridge Castle: Kitchen",
		"description": "Cooking stuff everywhere.",
		"exits": {
			"south": "rm_lumb_mainhallway"
		}
	},
	"rm_test_no_exits":
	{
		"name": "Test Room: No Exits",
		"description": "No exits here, chief.",
		"exits": null
    },
    "rm_test_all_exits":
    {
        "name": "Test Room: All Exits",
        "description": "All exits included and loop back here.",
        "exits": {
            "south": "rm_test_all_exits",
            "east": "rm_test_all_exits",
            "southwest": "rm_test_all_exits",
            "down": "rm_test_all_exits",
            "northwest": "rm_test_all_exits",
            "southeast": "rm_test_all_exits",
            "west": "rm_test_all_exits",
            "north": "rm_test_all_exits",
            "up": "rm_test_all_exits",
            "northeast": "rm_test_all_exits"
        }

    },
	"rm_test_items":
	{
		"name": "Test Room: A few items",
        "description": "Test out items here",
        "items": [items_db_json.basic.i_b_bucket, items_db_json.basic.i_b_egg, items_db_json.basic.i_b_milk, items_db_json.basic.i_b_wheat, items_db_json.basic.i_b_wheat, items_db_json.basic.i_b_potflour],
		"exits": {
			"south": "rm_test_kitchen"
		}
	},
	"rm_test_kitchen":
	{
		"name": "Test Room: Cook Something",
        "description": "Test out cooking",
        "npcs": [npcs_user_db_json.npc_q_questman],
		"items": [],
		"exits": {
            "north": "rm_test_items",
            "east": "rm_test_mill"
		}
    },
    "rm_test_mill":
    {
        "name": "Test Room: Mill Bottom Floor",
        "description": "Get flour here.",
        "exits": {
            "west": "rm_test_kitchen",
            "up": "rm_test_hopper"
        }
    },
    "rm_test_hopper":
    {
        "name": "Test Room: Mill Top Floor",
        "description": "Grind wheat here.",
        "exits": {
            "down": "rm_test_mill"
        }
    }

}

// Save changes made to rooms over the course of the game
// Will eventually check users save file as well
var rooms_user_json = rooms_initial_json;

class Location {
	constructor(rm) {
		this.cur_room = rooms_user_json[rm];
		this.write_descpription();
	}
    write_descpription() {
        $("#cur_room").text(this.cur_room.name);
        $("#txt_scrn").append("You are in " + "<b>" + this.cur_room.name + "</b>" + "<br>");
        $("#txt_scrn").append(this.cur_room.description + "<br>");
        //if (this.cur_room.objects != null) {
        //    $("#txt_scrn").append("Objects: " + this.cur_room.objects + "<br>");
        //}

        // Display the npcs
        var cur_npcs = this.cur_room["npcs"];
        if (cur_npcs != null) {
            for (var key in cur_npcs) {
                var npc_button_string = "<div class='btn btn-sm btn-outline-dark' onclick='cur_location.talk(" + key + ")'>";
                npc_button_string += cur_npcs[key].name;
                npc_button_string += "</div> ";
                npc_button_string += cur_npcs[key].flavor;
                npc_button_string += "<br>";
                $("#txt_scrn").append(npc_button_string);
            }
        }


        // Display Exits
        var exits_string = "You can go:<br>";
        var order_exits = ["up", "down", "northwest", "north", "northeast", "west", "east", "southwest", "south", "southeast"];

        // sort exits for consistancy between rooms
        var unsorted_exits = this.cur_room["exits"];

        // check if there are exits attached to the room
        if (unsorted_exits != null) {

            var exit_key_array = [];

            for (var key in unsorted_exits) {
                exit_key_array.push(key);
            }

            exit_key_array.sort(function (a, b) {
                return order_exits.indexOf(a) - order_exits.indexOf(b);
            });

            for (var i = 0; i < exit_key_array.length; i++) {
                var key_name = exit_key_array[i];
                var display_name = rooms_user_json[this.cur_room.exits[key_name]].name;
                //exits_string += "<button type='button' class='btn btn-outline-dark btn-sm' style='text-transform:capitalize' onclick='go(\"" + key_name + "\")'>" + key_name + "</button>" + " is " + display_name + "<br>";
                exits_string += "<div class='btn btn-dir btn-outline-dark btn-sm' style='text-transform:capitalize' onclick='go(\"" + key_name + "\")'>" + key_name + "</div> ";
            }

            exits_string += "<br> ";

            $("#txt_scrn").append(exits_string);
        }
		// Display ground items
		this.show_ground_items();

        this.textscroll();
    }

    textscroll() {
        $('#txt_scrn').animate({ scrollTop: $('#txt_scrn').prop("scrollHeight") - $('#txt_scrn').height() + 1 }, 300);
    }

	move(dir) {
		if (this.cur_room.exits[dir] != null) {
			var new_room = this.cur_room.exits[dir];
			this.cur_room = rooms_user_json[new_room];
			
            $("#txt_scrn").append("<br>You head " + dir + ".<hr>");

            // Disable and change btn-dir divs
            $('.btn-dir', '#txt_scrn').removeAttr("onclick");
            $('.btn-dir', '#txt_scrn').css('display', 'inline-block');
            $('.btn-dir', '#txt_scrn').css('color', 'gray');
            $('.btn-dir', '#txt_scrn').prepend("&nbsp;");
            $('.btn-dir', '#txt_scrn').append("&nbsp;");
            $('.btn-dir', '#txt_scrn').removeAttr('class');
            


			this.write_descpription();
		}
	}
	show_ground_items() {
		var gd_items = this.cur_room.items;
		$('#ground_row').html('');
        if (gd_items != null) {
            for (var i = 0; i < gd_items.length; i++) {
                var button_open = '<div class="btn btn-outline-success" href="#" role="button" onclick="cur_inv.take(' + i + ')" id="gd_item_' + i + '" data - toggle="dropdown" aria - haspopup="true" aria - expanded="false" style = "width: 100%" > ';
                var button_text = "<b>Take</b><br>" + gd_items[i].name.charAt(0).toUpperCase() + gd_items[i].name.slice(1);
                var button_close = '</div>';
                var button_finished = "<div class='col-6' style='padding:10px'>" + button_open + button_text + button_close + "</div>";

                $('#ground_row').append(button_finished);
            }
        }
		
    }

    talk(npc) {
        // check npc progress
        let cur_npc = this.cur_room.npcs[npc];
        let cur_progress = cur_npc.progress;
        let cur_speech = cur_npc.dlg_tree[cur_progress].text;
        let cur_requirement = cur_npc.dlg_tree[cur_progress].requirement;
        let next_progress = cur_npc.dlg_tree[cur_progress].progress;

        $("#txt_scrn").append("<br>-NPC-<br>" + cur_speech + "<br>");

        if (next_progress != null) {
            // check if requirement exist
            if (cur_requirement == null) {
                this.cur_room.npcs[npc].progress = next_progress;
            }
            else if (cur_inv.cur_items.length > 0) {
                let req_met = 0;
                let req_needed = cur_requirement.length;
                let req_inv_items = [];

                for (var i = 0; i < cur_requirement.length; i++) {
                    let i_require = cur_requirement[i];
                    console.log(cur_inv.cur_items);
                    console.log(cur_inv);
                    for (var g = 0; g < cur_inv.cur_items.length; g++) {
                        if (cur_inv.cur_items[g].name == i_require) {
                            req_inv_items.push(cur_inv.cur_items[g]);
                            req_met += 1;
                            break;
                        }
                    }
                }
                console.log(req_met + " / " + req_needed);
                if (req_met == req_needed) {
                    // take items from the player
                    for (var r = 0; r < req_inv_items.length; r++) {
                        for (var t = 0; t < cur_inv.cur_items.length; t++) {
                            if (req_inv_items[r].name == cur_inv.cur_items[t].name) {
                                cur_inv.cur_items.splice($.inArray(cur_inv.cur_items[t], cur_inv.cur_items), 1);
                                break;
                            }
                        }
                    }
                    cur_inv.show_inventory_items();
                    this.cur_room.npcs[npc].progress = next_progress;
                    //cur_speech = cur_npc.dlg_tree[this.cur_room.npcs[npc].progress].text;
                    //this.cur_room.npcs[npc].progress = cur_npc.dlg_tree[cur_progress + 1].progress;
                    //$("#txt_scrn").append("<br>-NPC-<br>" + cur_speech + "<br>");

                    this.talk(npc);
                }
            }
        }

        
        this.textscroll();
        //$("#txt_scrn").append('<hr>');
        //this.write_descpription();
    }
}

function go(d) {
	cur_location.move(d);
}

// Players initial location at game start
let cur_location = new Location("rm_test_kitchen");